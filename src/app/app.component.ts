import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	posts = [
		{
			title: 'first post',
			content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium eveniet perspiciatis dolorem, quia ipsam voluptate dolores dolor in pariatur, amet totam maiores, quo tempora animi vero fugiat earum cupiditate blanditiis.',
			loveIts: 0,
			created_at: new Date()
		},
		{
			title: 'second post',
			content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium eveniet perspiciatis dolorem, quia ipsam voluptate dolores dolor in pariatur, amet totam maiores, quo tempora animi vero fugiat earum cupiditate blanditiis.',
			loveIts: 0,
			created_at: new Date()
		},
		{
			title: 'third post',
			content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium eveniet perspiciatis dolorem, quia ipsam voluptate dolores dolor in pariatur, amet totam maiores, quo tempora animi vero fugiat earum cupiditate blanditiis.',
			loveIts: 0,
			created_at: new Date()
		}
	];
}
